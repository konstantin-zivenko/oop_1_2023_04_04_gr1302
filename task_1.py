class Books:
    def __init__(self, name, author, date, genre):
        self.name = name
        self.author = author
        self.date = date
        self.genre = genre

    def __str__(self):
        return f"book: {self.name}, author: {self.author}"

    def __eq__(self, other):
        return self.name == other.name and self.author == other.author


if __name__ == "__main__":
    b1 = Books("Mary", "King", 2020, "horor")
